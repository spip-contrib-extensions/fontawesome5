<?php
// Ceci est un fichier langue de SPIP -- This is a SPIP language file


if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// F
	'fontawesome5_description' => 'Font Awesome 5 dans le public et le privé de SPIP',
	'fontawesome5_nom' => 'Font Awesome 5',
	'fontawesome5_slogan' => 'Font Awesome 5 (version gratuite) en police de caractères',
);
