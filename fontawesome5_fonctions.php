<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}


/**
 * Lister le nom des classes mis à disposition par la librairie FontAwesome.
 *
 * @return array
 */
function lister_fontawesome() {
	$style_picto = find_in_path('fontawesome/css/all.css');
	$style_picto = file_get_contents($style_picto);
	$list = array();
	$liste = array();

	// on extrait toutes les classes d'icones
	preg_match_all("/\.fa-([[:alpha:]]+-?[[:alpha:]]+?):before/", $style_picto, $picto);
	if (isset($picto[1]) and is_array($picto[1]) and count($picto[1])) {
		natsort($picto[1]);
		$list = array_merge($list, $picto[1]);
	}
	natsort($list);

	// on recherche le prefixe associé fab ou fas
	foreach ($list as $k => $v) {
		$prefixe = (in_array($v, $GLOBALS['fontawesome_brands'])) ? 'fab' : 'fas';
		$liste[] = "$v $prefixe";
	}

	return $liste;
}


