<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

function fontawesome5_header_prive($flux) {
	include_spip('inc/utils');
	$flux .= "\n" . '<link rel="stylesheet" type="text/css" media="all" href="' . find_in_path('fontawesome/css/all.min.css') . '" />';
	return $flux;
}

function fontawesome5_insert_head_css($flux) {
	include_spip('inc/utils');
	$flux .= "\n" . '<link rel="stylesheet" type="text/css" media="all" href="' . find_in_path('fontawesome/css/all.min.css') . '" />';
	return $flux;
}

